import 'dart:async';
import 'dart:convert';
import 'package:distribucion/api/model/model_index.dart';
//import 'package:requests/requests.dart';
import 'package:http/http.dart' as http;
import 'dart:core';

class RequestPost {
  Future<DataApi> httpPost(String url, String dato) async {
    // print("enviar dato");
    final response = await http.post(
      Uri.parse(url),
      body: {
        "Dato": dato
      },
    );
    // if (response.statusCode == 201) {
    if (response.statusCode >= 200 && response.statusCode < 300) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      return DataApi.fromJson(jsonDecode(response.body));
    } else {
      // print("$response.status");
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      //throw Exception('Fallo la Solicitud');
      throw ApiResultError.fromJson(jsonDecode(response.body));
    }
  }
}
