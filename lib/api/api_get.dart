import 'dart:async';
import 'dart:convert';
import 'package:distribucion/api/model/model_index.dart';
//import 'package:requests/requests.dart';
import 'package:http/http.dart' as http;
import 'dart:core';

class RequestGet {
  Future<dynamic> httpGet(String url) async {
    // print("enviar dato");
    final response = await http.get(Uri.parse(url));
    // url);
    // if (response.statusCode == 201) {
    if (response.statusCode >= 200 && response.statusCode < 300) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      //   print(response.body);
      return jsonDecode(response.body);
    } else {
      // print("$response.status");
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      //throw Exception('Fallo la Solicitud');
      throw ApiResultError.fromJson(jsonDecode(response.body));
    }
  }
}
