class DataApi {
  final String? comando;
  final String? tipo;
  final String? status;
  final dynamic? dato;

  DataApi({this.comando, this.tipo, this.status, this.dato});

  factory DataApi.fromJson(Map<String, dynamic> json) {
    return DataApi(
      comando: json['comando'],
      tipo: json['tipo'],
      status: json['Status'],
      dato: json['Dato'],
    );
  }
}
