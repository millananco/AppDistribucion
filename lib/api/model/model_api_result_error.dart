class ApiResultError {
  const ApiResultError({this.message});

  final String? message;

  static ApiResultError fromJson(dynamic json) {
    return ApiResultError(
      message: json['message'] as String,
    );
  }
}
