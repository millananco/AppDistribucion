import 'package:distribucion/api/api-post03.dart';
import 'dart:core';

class ApiPost extends RequestPost {
  static final ApiPost _singleton = new ApiPost._internal();
  factory ApiPost() {
    return _singleton;
  }
  ApiPost._internal();
}
