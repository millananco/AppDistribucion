import 'dart:async';
//import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:distribucion/sotrMeter/models/models_meter.dart';
import 'package:distribucion/sotrMeter/views/widgets/modbus01.dart';
import 'package:distribucion/presentation/layouts/layout_dinamic02.dart';

class ViewModbus extends StatefulWidget {
  const ViewModbus({Key? key, this.meter}) : super(key: key);
  final ModelMeter? meter;
  @override
  // _HomePageState createState() => _HomePageState();
  State<ViewModbus> createState() => _HomePageState(meter!);
}

int tsAnterior = 1;
int mystatus = 1;

class _HomePageState extends State<ViewModbus> {
  ModelMeter _meter;
  _HomePageState(this._meter); //constructor

  final textcontroller = TextEditingController();
  final tECFunctionCode = TextEditingController();
  final tECStartAddress = TextEditingController();
  final tECNoOfRegisters = TextEditingController();

  late DatabaseReference _dataCommand;
  late DatabaseReference _dataInstrumentation;
  late StreamSubscription<DatabaseEvent> _counterSubscription;
  FirebaseException? _error;
  //final Future<FirebaseApp> _future = Firebase.initializeApp();
  dynamic? data;
  bool hayData = false;
  @override
  void initState() {
    _init();

    super.initState();
  }

  Future<String> _getValue() async {
    await Future.delayed(Duration(seconds: 3));
    return 'Woolha';
  }

  void _init() {
    String urlEnergia = 'SOTR/IdDevice/' + _meter.tokenGateway! + '/Data/IdModbus/' + _meter.idModbus! + '/ModBusRTU';
    _dataInstrumentation = FirebaseDatabase.instance.ref(urlEnergia);

    _counterSubscription = _dataInstrumentation.onValue.listen(
      (DatabaseEvent event) {
        setState(() {
          if (event.snapshot.value == null) {
            print('Los datos son null');
          } else {
            _error = null;
            print('Dato Ahora : ${event.snapshot.value}');
            hayData = true;
            data = event.snapshot.value;
            final String energia = data['Data'];
            final int ts = data['Ts'];
            print('Energia:' + energia);
            print('TS:' + ts.toString());
          }
        });
      },
      onError: (Object o) {
        final error = o as FirebaseException;
        setState(() {
          _error = error;
        });
      },
    );
  }

  void addData(String functionCode, String startAddress, String noOfRegisters, String myTypeData) {
    String urlComando = 'SOTR/IdDevice/' + _meter.tokenGateway! + '/Command';
    _dataCommand = FirebaseDatabase.instance.ref(urlComando);
    _dataCommand.set({
      //"{\"comando\":\"ModBus\",\"tipo\":\"SetOpenClosed\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":"+String(auxMeter)+",\"StateMeter\":\"Open\"}}"
      //"{\"comando\":\"ModBus\",\"tipo\":\"SetOpenClosed\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":"+String(auxMeter)+",\"StateMeter\":\"Close\"}}";
      'Data': "{\"comando\":\"ModBus\",\"tipo\":\"ModBusRTU\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":\"" + _meter.idModbus! + "\",\"IdFuncion\":\"" + functionCode + "\",\"Address\":\"" + startAddress + "\",\"NroDatos\":\"" + noOfRegisters + "\",\"TipoDato\":\"5\"}}",
      //'Data': "{\"comando\":\"ModBus\",\"tipo\":\"SetOpenClosed\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":\"" + _meter.idModbus! + "\",\"StateMeter\":\"Open\"}}", // data,
      'TS': ServerValue.timestamp
    });
    setState(() {
      mystatus = 2;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool? progressindicator;
    Widget? myWidget;
    if (hayData) {
      dynamic? dato;
      dato = data;
      final String dataModbus = dato['Data'];
      final int ts = dato['Ts'];
      print('Modbus:' + dataModbus);
      print('TS:' + ts.toString());
      // final ModelCorteReconexion mydata = ModelCorteReconexion.fromJson(json.decode(energia));
      // final dynamic myenergy = json.decode(energia1);
      // print('EstadoMedidor : ${mydata.statemeter}');
      final newYearsEve = DateTime.fromMillisecondsSinceEpoch(ts, isUtc: false);
      print(newYearsEve);
      if ((tsAnterior != ts) || (mystatus == 1)) {
        tsAnterior = ts;
        mystatus = 1;
        progressindicator = false;
      } else {
        progressindicator = true;
      }
      myWidget = WidgetModbus(lastSync: '${newYearsEve}', myModbus: dataModbus, cTECFunctionCode: tECFunctionCode, cTECStartAddress: tECStartAddress, cTECNoOfRegisters: tECNoOfRegisters, btActualizar: () => addData(tECFunctionCode.text, tECStartAddress.text, tECNoOfRegisters.text, '5'));
    } else {
      print('Entro en:snapshot.hasError');
      myWidget = Center(child: Text('Error No hay datos...'));
      progressindicator = true;
    }

    return Scaffold(
        appBar: AppBar(
          title: Text("Modbus"),
        ),
        body: LayoutDinamic(
          child: myWidget,
          progressindicator: progressindicator,
        ));
  }
}
