import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:distribucion/sotrMeter/models/models_instrumentation.dart';
import 'package:distribucion/sotrMeter/views/widgets/instrumentacion01.dart';
import 'package:distribucion/sotrMeter/views/widgets/instrumentacionBT.dart';
import 'package:distribucion/sotrMeter/models/models_meter.dart';
import 'package:distribucion/presentation/layouts/layout_dinamic02.dart';

class ViewInstrumentation extends StatefulWidget {
  const ViewInstrumentation({Key? key, this.meter}) : super(key: key);
  final ModelMeter? meter;
  @override
  // _HomePageState createState() => _HomePageState();
  State<ViewInstrumentation> createState() => _HomePageState(meter!);
}

int tsAnterior = 1;
int mystatus = 1;

class _HomePageState extends State<ViewInstrumentation> {
  ModelMeter _meter;
  _HomePageState(this._meter); //constructor

  final textcontroller = TextEditingController();

  late DatabaseReference _dataCommand;
  late DatabaseReference _dataInstrumentation;
  late StreamSubscription<DatabaseEvent> _counterSubscription;
  FirebaseException? _error;
  // final Future<FirebaseApp> _future = Firebase.initializeApp();
  dynamic? data;
  bool hayData = false;
  @override
  void initState() {
    _init();

    super.initState();
  }

  Future<String> _getValue() async {
    await Future.delayed(Duration(seconds: 3));
    return 'Woolha';
  }

  void _init() {
    String urlEnergia = 'SOTR/IdDevice/' + _meter.tokenGateway! + '/Data/IdModbus/' + _meter.idModbus! + '/SotrInstrumentacion';
    _dataInstrumentation = FirebaseDatabase.instance.ref(urlEnergia);

    _counterSubscription = _dataInstrumentation.onValue.listen(
      (DatabaseEvent event) {
        setState(() {
          _error = null;
          print('Dato Ahora : ${event.snapshot.value}');
          hayData = true;
          data = event.snapshot.value;
          final String energia = data['Data'];
          final int ts = data['Ts'];
          print('Energia:' + energia);
          print('TS:' + ts.toString());
        });
      },
      onError: (Object o) {
        final error = o as FirebaseException;
        setState(() {
          _error = error;
        });
      },
    );
  }

  void _addData() {
    String urlComando = 'SOTR/IdDevice/' + _meter.tokenGateway! + '/Command';
    _dataCommand = FirebaseDatabase.instance.ref(urlComando);
    _dataCommand.set({
      'Data': "{\"comando\":\"ModBus\",\"tipo\":\"SotrInstrumentacion\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\": " + _meter.idModbus! + "}}", // data,
      'TS': ServerValue.timestamp
    });
    setState(() {
      mystatus = 2;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool? progressindicator;
    Widget? myWidget;
    List<Widget> children;

    if (hayData) {
      dynamic? dato;
      dato = data;
      final String energia = dato['Data'];
      final int ts = dato['Ts'];
      print('Energia:' + energia);
      print('TS:' + ts.toString());

      final ModelInstrumentation dataInstrumentation = ModelInstrumentation.fromJson(json.decode(energia));
      // final dynamic myenergy = json.decode(energia1);
      final newYearsEve = DateTime.fromMillisecondsSinceEpoch(ts, isUtc: false);
      print(newYearsEve);

      print('Tension VL12 : ${dataInstrumentation.vl12}');
      if ((tsAnterior != ts) || (mystatus == 1)) {
        tsAnterior = ts;
        mystatus = 1;
        progressindicator = false;
      } else {
        progressindicator = true;
      }
      if ((_meter.idTipoMedidor! == "2") || (_meter.idTipoMedidor! == "4"))
        myWidget = WidgetInstrumentacion(lastSync: '${newYearsEve}', instrumentation: dataInstrumentation, btActualizar: () => _addData());
      else
        myWidget = WidgetInstrumBT(lastSync: '${newYearsEve}', instrumentation: dataInstrumentation, btActualizar: () => _addData());

      children = <Widget>[
        const Icon(
          Icons.check_circle_outline,
          color: Colors.green,
          size: 60,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 16),
          child: Text('Result: ${data}'),
        ),
        SizedBox(height: 250.0),
        Padding(
          padding: EdgeInsets.all(10.0),
          child: TextField(
            controller: textcontroller,
          ),
        ),
        SizedBox(height: 30.0),
        Center(
            child: RaisedButton(
                color: Colors.pinkAccent,
                child: Text("Save to Database"),
                onPressed: () {
                  // addData(textcontroller.text);
                  //call method flutter upload
                })),
      ];
    } else {
      print('Entro en:snapshot.hasError');
      myWidget = Center(child: Text('Error No hay datos...'));
      progressindicator = true;
    }
    return Scaffold(
        appBar: AppBar(
          title: Text("Instrumentacion"),
        ),
        body: LayoutDinamic(
          child: myWidget,
          progressindicator: progressindicator,
        ));
  }
}
