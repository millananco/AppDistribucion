import 'package:flutter/material.dart';
import 'package:distribucion/presentation/widgets/widget_index.dart';
import 'package:distribucion/sotrMeter/views/widgets/item_data.dart';
import 'package:distribucion/sotrMeter/views/widgets/item_textInput.dart';
import 'dart:core';

class WidgetModbus extends StatelessWidget {
  const WidgetModbus({Key? key, this.lastSync, this.myModbus, required this.cTECFunctionCode, required this.cTECNoOfRegisters, required this.cTECStartAddress, required this.btActualizar}) : super(key: key);
  final String? lastSync;
  //final ModelEnergy? myModbus;
  final String? myModbus;
  final void Function()? btActualizar;
  final TextEditingController? cTECFunctionCode;
  final TextEditingController? cTECStartAddress;
  final TextEditingController? cTECNoOfRegisters;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
        child: Column(
      children: <Widget>[
        SizedBox(
          height: 5,
        ),
        ItemData('Last Sync', lastSync!),
        ItemData('ModBus Data', '${myModbus}'),
        ItemInputText(
          hint: 'Function Code',
          title: 'Function Code',
          inputType: TextInputType.number,
          inputAction: TextInputAction.next,
          cTextImput: cTECFunctionCode,
        ),
        ItemInputText(
          hint: 'Start Address',
          title: 'Start Address',
          inputType: TextInputType.number,
          inputAction: TextInputAction.next,
          cTextImput: cTECStartAddress,
        ),
        ItemInputText(
          hint: 'No of Registers',
          title: 'No of Registers',
          inputType: TextInputType.number,
          inputAction: TextInputAction.done,
          cTextImput: cTECNoOfRegisters,
        ),
        SizedBox(
          height: 10,
        ),
        RoundedButton(
          buttonName: 'Actualizar',
          buttonAction: btActualizar,
          radius: 1,
          /*  buttonAction:() => myhttpPost(context)*/
        ),
        SizedBox(
          height: 5,
        ),
      ],
    ));
  }
}
