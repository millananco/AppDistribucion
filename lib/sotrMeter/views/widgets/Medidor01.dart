import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
//import 'package:coopeluz/pallete.dart';
import 'package:distribucion/presentation/widgets/widget_index.dart';
import 'dart:convert';

import 'package:distribucion/api/api_get.dart';

class Medidor01 extends StatelessWidget {
  final String? lavel;
  final String? idModbus;
  final String? numeroSerie;
  final String? estado;
  final String? tokenGateway;
  const Medidor01({Key? key, this.lavel, this.idModbus, this.numeroSerie, this.estado, this.tokenGateway}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final RequestPost requestPost = new RequestPost();
    //https://coopeluzweb-621e7.firebaseio.com/EquipoId/ee394e83a6134e3e514f55e73ecb68d7e1f1294e.json
    //https://coopeluzweb-621e7.firebaseio.com/EquipoId/ee394e83a6134e3e514f55e73ecb68d7e1f1294e/IdModBus/1/Datos
    final RequestGet requestGet = new RequestGet();

    String myurl = "https://coopeluzweb-621e7.firebaseio.com/EquipoId/" + tokenGateway! + "/IdModBus/" + idModbus! + "/Datos.json";
    print(myurl);
    String titulo = "Medidor " + lavel!;
    return Stack(children: [
      /*   BackgroundImage(
        image: 'assets/images/appTE.jpg',
      ),
*/
      Scaffold(
          //  backgroundColor: Colors.transparent,
          appBar: AppBar(
            // backgroundColor: Color(0xff5663ff).withOpacity(0.5),
            title: Text(titulo),
          ),
          body: LayoutBuilder(builder: (BuildContext context, BoxConstraints viewportConstraints) {
            return SingleChildScrollView(
                child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: viewportConstraints.maxHeight,
                    ),
                    child: Column(
                      children: [
                        Text("NS:" + numeroSerie!, style: TextStyle(fontSize: 22, height: 1.5)),
                        Text("IdModbus:" + idModbus!, style: TextStyle(fontSize: 22, height: 1.5)),
                        WidgetMedidor(dataGet: requestGet.httpGet(myurl)),
                        // child: MyApp(dataGet: httpGet(myurl)),
                      ],
                    )));
          }))
    ]);
  }
}

class WidgetMedidor extends StatelessWidget {
  final Future<dynamic>? dataGet;
  //final Future<DataGet> dataGet;

  WidgetMedidor({Key? key, this.dataGet}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FutureBuilder<dynamic>(
        future: dataGet,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return RequestMedidor(datos: snapshot.data); //Text(snapshot.data.status);
          } else if (snapshot.hasError) {
            return Text("Error en la conexion"); //Text("${snapshot.error}");
          }
          // Por defecto, muestra un loading spinner
          return CircularProgressIndicator();
        },
      ),
    );
  }
}

class RequestMedidor extends StatelessWidget {
  final dynamic? datos;

  RequestMedidor({this.datos});
  @override
  Widget build(BuildContext context) {
    Funciones mifuncion = new Funciones();
    print('llegaron estos datos: ${datos}');

    String energia = datos['Energia'];
    //  print("Energia:" + energia);
    String estadoOpenClose = datos['EstadoOpenClose'];
    //print("estadoOpenClose:" + estadoOpenClose);
    String instrumentacion = datos['Instrumentacion'];
    //print(" instrumentacion:" + instrumentacion);
    String modbusRtu = datos['ModbusRtu'];
    //print("modbusRtu:" + modbusRtu);
    return Column(children: [
      // Text("Energia", style: TextStyle(fontSize: 22, height: 1.5)),
      // Text(mifuncion.fcEnergia(energia), style: TextStyle(fontSize: 22, height: 1.5)),
      //  Text(energia,style:kBodyText),
      Text("Instrumentacion", style: TextStyle(fontSize: 22, height: 1.5)),
      Text(mifuncion.fcInstrumentacion(instrumentacion), style: TextStyle(fontSize: 22, height: 1.5)),
      // Text(instrumentacion,style:kBodyText),
      Text("EstadoMedidor", style: TextStyle(fontSize: 22, height: 1.5)),
      // Text(estadoOpenClose,style:kBodyText),
      Text(mifuncion.fcEstadoRelay(estadoOpenClose), style: TextStyle(fontSize: 22, height: 1.5)),
      Text("ComandoModbus", style: TextStyle(fontSize: 22, height: 1.5)),
      // Text(modbusRtu,style:kBodyText),
      Text(mifuncion.comandoMotbus(modbusRtu), style: TextStyle(fontSize: 22, height: 1.5)),
    ]);
  }
}

class Funciones {
  String fcEnergia(String dato) {
    String mydato = dato.replaceAll("'", '"');
    print("Energia:" + mydato);
    /*
      var jsonData = '{ "name" : "Dane", "alias" : "FilledStacks"  }';
    var parsedJson = json.decode(jsonData);
    print('${parsedJson.runtimeType} : $parsedJson');
    print(mydato); 
    */

    dynamic jsonDato = jsonDecode(mydato);
    // print('$jsonDato');
    print("Energia:" + mydato);
    String energia;
    energia = "Energia Import " + jsonDato['Dato']['Energia']['Import'] + "\n";
    print("Energia1:" + energia);
    energia += "Energia Export " + jsonDato['Dato']['Energia']['Export'] + "\n" + "Reactiva Import " + jsonDato['Dato']['Reactiva']['Import'] + "\n" + "Reactiva import " + jsonDato['Dato']['Reactiva']['Export'] + "\n" + "Demanda Watts " + jsonDato['Dato']['Demanda']['MaximaWatts'] + "\n" + "Demanda VA " + jsonDato['Dato']['Demanda']['MaximaVA'] + "\n" + "VoltAmperHora " + jsonDato['Dato']['VoltAmperHora'];
    print("Energia2:" + energia);
    return (energia);
  }

  String fcEstadoRelay(String dato) {
    String mydato = dato.replaceAll("'", '"');
    dynamic jsonDato = jsonDecode(mydato);
    String statemeter;
    statemeter = "Estado Relays: " + jsonEncode(jsonDato['Dato']['StatusOutput']) + '\n' + "Estado Medidor: " + jsonDato['Dato']['StateMeter'];
    return (statemeter);
  }

  String fcInstrumentacion(String dato) {
    String mydato = dato.replaceAll("'", '"');
    dynamic jsonDato = jsonDecode(mydato);
    String instrumentacion;
    instrumentacion = "Tension Promedio " + jsonDato['Dato']['Vpromedio'] + '\n' + "Corriente Promedio " + jsonDato['Dato']['Ipromedio'] + '\n' + "Sumatoria Corrientes  " + jsonDato['Dato']['SumaIl'] + '\n' + "Potencia " + jsonDato['Dato']['Pt'] + '\n' + "Aparente " + jsonDato['Dato']['VA'] + '\n' + "Reactiva " + jsonDato['Dato']['VAr'] + '\n' + "CosQ " + jsonDato['Dato']['CosQ'] + '\n' + "Frecuencia " + jsonDato['Dato']['Frecuencia'];
    return (instrumentacion);
  }

  String comandoMotbus(String dato) {
    String mydato = dato.replaceAll("'", '"');
    print(mydato);
    dynamic jsonDato = jsonDecode(mydato);
    print('$jsonDato');

    String comandomotbus;
    comandomotbus = "IdModbus " + jsonDato['Dato']['IdDireccion'].toString() + "\n";
    //print(comandomotbus);
    comandomotbus += "IdFuncion: " + jsonDato['Dato']['IdFuncion'].toString() + '\n' + "Address:  " + jsonDato['Dato']['Address'].toString() + '\n' + "NroDatos: " + jsonDato['Dato']['NroDatos'].toString() + '\n' + "Datos: " + jsonEncode(jsonDato['Dato']['DatosModbus']);
    //print(comandomotbus);
    return (comandomotbus);
  }
}
/*
este es el json de la respuesta
{"comando":"usuario",
"tipo":"UsuarioMedidor",
"Status":"Req",
"Dato":{"Medidor":
[[{"Id":"3","tokenGateway":"95d9c56ab2bb26986049adde2e4323bca41df872","Lavel":"Informatica","IdLoraSlave":"0","IdModbus":"1","NumeroSerie":"282210148","IdTipoMedidor":"1","Suministro":"0","Estado":"1","FechaHora":"2021-03-10 12:01:40"}],
[{"Id":"2","tokenGateway":"ee394e83a6134e3e514f55e73ecb68d7e1f1294e","Lavel":"Laboratorio","IdLoraSlave":"0","IdModbus":"1","NumeroSerie":"01232912","IdTipoMedidor":"2","Suministro":"0","Estado":"1","FechaHora":"2021-01-13 09:57:08"}]]}}';
*/
