import 'package:flutter/material.dart';
import 'package:distribucion/presentation/widgets/widget_index.dart';
import 'package:distribucion/sotrMeter/views/widgets/item_data.dart';
import 'package:distribucion/sotrMeter/models/models_energy.dart';
import 'dart:core';

class WidgetEnergia extends StatelessWidget {
  const WidgetEnergia({Key? key, this.lastSync, this.myenergy, required this.btActualizar}) : super(key: key);
  final String? lastSync;
  final ModelEnergy? myenergy;
  final void Function()? btActualizar;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
        child: Column(
      children: <Widget>[
        SizedBox(
          height: 5,
        ),
        ItemData('Last Sync', lastSync!),
        ItemData('Energy Import', myenergy!.energyImport!),
        ItemData('Energy Export', myenergy!.energyExport!),
        ItemData('Reactive Import', myenergy!.reactiveImport!),
        ItemData('Reactive Export', myenergy!.reactiveExport!),
        ItemData('Energy Apparent', myenergy!.voltAh!),
        ItemData('Demanda Max Watts', myenergy!.demandMaxWatts!),
        ItemData('Demanda Max VA', myenergy!.demandMaxVA!),
        SizedBox(
          height: 10,
        ),
        RoundedButton(
          buttonName: 'Actualizar',
          buttonAction: btActualizar,
          radius: 1,
          /*  buttonAction:() => myhttpPost(context)*/
        ),
        SizedBox(
          height: 5,
        ),
      ],
    ));
  }
}
