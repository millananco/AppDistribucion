import 'package:flutter/material.dart';
import 'package:distribucion/sotrMeter/models/models_meter.dart';
//import 'package:distribucion/sotrMeter/views/widgets/Medidor01.dart';
import 'package:distribucion/sotrMeter/views/widgets/widget_meter01.dart';
import 'package:flutter/cupertino.dart';

class ListMeter extends StatelessWidget {
  ListMeter({
    Key? key,
    required this.listMeter,
  }) : super(key: key);

  final List<ModelMeter> listMeter;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      primary: false,
      itemBuilder: (BuildContext context, int index) => new ListItemMedidor(listMeter[index]),
      itemCount: listMeter.length,
      shrinkWrap: true,
    );
  }
}

class ListItemMedidor extends StatelessWidget {
  final ModelMeter _myMeter;
  ListItemMedidor(this._myMeter);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: ListTile(
      leading: Icon(CupertinoIcons.gauge, size: 50.0), //FlutterLogo(size: 56.0),
      title: Text(
        _myMeter.lavel!,
      ),
      subtitle: SingleChildScrollView(
        child: Text(
          "NS: " + _myMeter.numeroSerie! + "\r\n" + "IdModbus: " + _myMeter.idModbus! + "\r\n" + "Estado: " + _myMeter.estado!,
        ),
      ),
      trailing: Icon(Icons.more_vert),
      onTap: () {
        print('llegamos aca ' + _myMeter.idTipoMedidor!);
        Navigator.push(
          context,
          // MaterialPageRoute(builder: (context) => Medidor01(lavel: _lavel, idModbus: _idModbus, numeroSerie: _numeroSerie, estado: _estado, tokenGateway: _tokenGateway)),
          MaterialPageRoute(builder: (context) => WidgetMeter(meter: _myMeter)),
        );
      },
    ));
  }
}
