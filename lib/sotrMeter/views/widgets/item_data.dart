import 'package:flutter/material.dart';

class ItemData extends StatelessWidget {
  final String _title;
  final String _data;
  ItemData(this._title, this._data);

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Container(
        width: _size.width * 0.98,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(1),
          border: Border.all(width: 2.0, color: Colors.black38),
          color: Colors.white12,
        ),
        child: Column(children: <Widget>[
          SizedBox(
            height: 5,
          ),
          Text(
            _title,
            //  style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            _data,
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 5,
          ),
        ]));
  }
}
