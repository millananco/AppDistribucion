import 'package:flutter/material.dart';
import 'dart:core';

class ItemInputText extends StatelessWidget {
  ItemInputText({
    Key? key,
    required this.hint,
    this.title,
    this.inputType,
    this.inputAction,
    this.cTextImput,
  }) : super(key: key);

  final String? title;
  final String hint;
  final TextInputType? inputType;
  final TextInputAction? inputAction;
  final TextEditingController? cTextImput;
  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Container(
        width: _size.width * 0.98,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(1),
          border: Border.all(width: 2.0, color: Colors.black38),
          color: Colors.white12,
        ),
        child: Column(children: <Widget>[
          SizedBox(
            height: 5,
          ),
          Text(
            title!,
            //  style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
              width: _size.width * 0.60,
              child: TextField(
                controller: cTextImput,
                decoration: InputDecoration(
                  //border: InputBorder.none,
                  border: const OutlineInputBorder(),
                  prefixIcon: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  ),
                  hintText: hint,
                  hintStyle: TextStyle(fontSize: 20),
                ),
                style: TextStyle(fontSize: 20),
                keyboardType: inputType,
                textInputAction: inputAction,
              )),
          SizedBox(
            height: 5,
          ),
        ]));
  }
}
