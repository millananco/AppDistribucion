import 'package:flutter/material.dart';
import 'package:distribucion/presentation/widgets/widget_index.dart';
//import 'package:di/presentation/screens/suministro/widgets/suministro_saldo.dart';
import 'dart:core';
import 'package:distribucion/sotrMeter/models/models_meter.dart';
import 'package:distribucion/sotrMeter/views/views_energy01.dart';
import 'package:distribucion/sotrMeter/views/views_instrumentation01.dart';
import 'package:distribucion/sotrMeter/views/views_corte_reconexion01.dart';
import 'package:distribucion/sotrMeter/views/views_modbus01.dart';
import 'package:flutter/cupertino.dart';

class WidgetMeter extends StatelessWidget {
  const WidgetMeter({Key? key, this.meter}) : super(key: key);
  final ModelMeter? meter;
  // final Function? btMenu;

  @override
  Widget build(BuildContext context) {
    // Size size = MediaQuery.of(context).size;
    List<ListEntity>? items = [];
    if (meter!.idTipoMedidor! == "3") {
      items.add(ListEntity(icon: CupertinoIcons.gauge, title: "Instrumentacion", data: "Leer datos de Instrumentacion", functionAction: ViewInstrumentation(meter: meter)));
      items.add(ListEntity(icon: Icons.bar_chart, title: "Energia", data: "Leer datos de energia", functionAction: ViewEnergy(meter: meter)));
      items.add(ListEntity(icon: Icons.account_tree_rounded, title: "Modbus", data: "Comandos modbus ", functionAction: ViewModbus(meter: meter)));
      items.add(ListEntity(icon: CupertinoIcons.clear_circled, title: "Corte Suministro", data: "Cortar suministro", functionAction: ViewCorteReconexion(meter: meter)));
    } else if (meter!.idTipoMedidor! == "4") {
      items.add(ListEntity(icon: CupertinoIcons.gauge, title: "Instrumentacion", data: "Instrumentacion de Reconectador", functionAction: ViewInstrumentation(meter: meter)));
      items.add(ListEntity(icon: Icons.account_tree_rounded, title: "Modbus", data: "Comandos modbus ", functionAction: ViewModbus(meter: meter)));
      items.add(ListEntity(icon: CupertinoIcons.clear_circled, title: "ManiobrarReco", data: "Maniobras en Reconectador", functionAction: ViewCorteReconexion(meter: meter)));
    } else {
      items.add(ListEntity(icon: CupertinoIcons.gauge, title: "Instrumentacion", data: "Leer datos de Instrumentacion", functionAction: ViewInstrumentation(meter: meter)));
      items.add(ListEntity(icon: Icons.bar_chart, title: "Energia", data: "Leer datos de energia", functionAction: ViewEnergy(meter: meter)));
      items.add(ListEntity(icon: Icons.account_tree_rounded, title: "Modbus", data: "Comandos modbus ", functionAction: ViewModbus(meter: meter)));
    }
    return Scaffold(
        //  backgroundColor: Colors.transparent,
        appBar: AppBar(
          // backgroundColor: Color(0xff5663ff).withOpacity(0.5),
          title: Text("Medidor"),
        ),
        body: Container(
            //  padding: const EdgeInsets.only(top: 1.0),
            child: Column(
          children: <Widget>[
            //MyZisedBar(),
            /*   RoundedButton(
          buttonName: 'Menu',
          buttonAction: btMenu,
        ),*/
            Card(
                child: ListTile(
              title: Text(meter!.lavel!),
              subtitle: Text(
                'NS: ' + meter!.numeroSerie! + '\n' + 'IdModbus: ' + meter!.idModbus! + '\n' + 'Estado: ' + meter!.estado!,
              ),
              // Container(height: 200, child: SimpleBarChart.withSampleData()),
            )),
            ListViewScroll(mylist: items),
            SizedBox(
              height: 10,
            ),

            SizedBox(
              height: 5,
            ),
          ],
        )));
  }
}
