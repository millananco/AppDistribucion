/*Nota: Intrumentacion para baja tension*/
import 'package:flutter/material.dart';
import 'package:distribucion/presentation/widgets/widget_index.dart';
import 'package:distribucion/sotrMeter/views/widgets/item_data.dart';
import 'package:distribucion/sotrMeter/models/models_energy.dart';
import 'package:distribucion/sotrMeter/models/models_instrumentation.dart';

import 'dart:core';

class WidgetInstrumBT extends StatelessWidget {
  const WidgetInstrumBT({Key? key, this.lastSync, this.instrumentation, required this.btActualizar}) : super(key: key);
  final String? lastSync;
  final ModelInstrumentation? instrumentation;
  final void Function()? btActualizar;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
        child: Column(
      children: <Widget>[
        SizedBox(
          height: 5,
        ),
        ItemData('Last Sync', lastSync!),
        ItemDataInstr('Tension', 'VL1  ' + instrumentation!.vl12!, 'VL2  ' + instrumentation!.vl23!, 'VL3  ' + instrumentation!.vl31!),
        ItemDataInstr('Current', 'IL1  ' + instrumentation!.il1!, 'IL2  ' + instrumentation!.il2!, 'IL3  ' + instrumentation!.il3!),
        ItemDataInstr('Cosine', 'CosQL1  ' + instrumentation!.cosql1!, 'CosQL2  ' + instrumentation!.cosql2!, 'CosQL3  ' + instrumentation!.cosql3!),
        SizedBox(
          height: 10,
        ),
        RoundedButton(
          buttonName: 'Actualizar',
          buttonAction: btActualizar,
          radius: 1,
        ),
        SizedBox(
          height: 5,
        ),
      ],
    ));
  }
}

class ItemDataInstr extends StatelessWidget {
  final String _title;
  final String _data1;
  final String _data2;
  final String _data3;
  ItemDataInstr(this._title, this._data1, this._data2, this._data3);

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Container(
        width: _size.width * 0.98,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(1),
          border: Border.all(width: 2.0, color: Colors.black38),
          color: Colors.white12,
        ),
        child: Column(children: <Widget>[
          SizedBox(
            height: 5,
          ),
          Text(
            _title,
            //  style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            _data1,
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            _data2,
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            _data3,
            style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 5,
          ),
        ]));
  }
}
