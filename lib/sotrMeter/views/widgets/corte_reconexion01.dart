import 'package:flutter/material.dart';
import 'package:distribucion/presentation/widgets/widget_index.dart';
import 'package:distribucion/sotrMeter/views/widgets/item_data.dart';
import 'package:distribucion/sotrMeter/models/models_corte_reconexion.dart';
import 'dart:core';

class WidgetCorteReconexion extends StatelessWidget {
  const WidgetCorteReconexion({Key? key, this.lastSync, this.corteReconexion, required this.btCorte, required this.btReconexion}) : super(key: key);
  final String? lastSync;
  final ModelCorteReconexion? corteReconexion;
  final void Function()? btCorte;
  final void Function()? btReconexion;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    if (corteReconexion!.statemeter == "Open") {
      return Center(
          child: Column(
        children: <Widget>[
          SizedBox(
            height: 5,
          ),
          ItemData('Last Sync', lastSync!),
          ItemDataCorte('Status Meter', 'Suministro Cortado', Colors.red),
          ItemData('Status RelayL1', corteReconexion!.statusl1!),
          ItemData('Status RelayL2', corteReconexion!.statusl2!),
          ItemData('Status RelayL3', corteReconexion!.statusl3!),
          SizedBox(
            height: 10,
          ),
          RoundedButton(
            buttonName: 'Conectar Suministro',
            buttonAction: btReconexion,
            radius: 1,
            /*  buttonAction:() => myhttpPost(context)*/
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ));
    } else {
      return Center(
          child: Column(
        children: <Widget>[
          SizedBox(
            height: 5,
          ),
          ItemData('Last Sync', lastSync!),
          ItemDataCorte('Status Meter', 'Suministro Normal', Colors.green),
          ItemData('Status RelayL1', 'Closed'), //corteReconexion!.statusl1!),
          ItemData('Status RelayL2', 'Closed'), //corteReconexion!.statusl2!),
          ItemData('Status RelayL3', 'Closed'), //corteReconexion!.statusl3!),
          SizedBox(
            height: 10,
          ),
          RoundedButton(
            buttonName: 'Cortar Suministro',
            buttonAction: btCorte,
            radius: 1,
            /*  buttonAction:() => myhttpPost(context)*/
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ));
    }
  }
}

class ItemDataCorte extends StatelessWidget {
  final String _title;
  final String _data;
  final Color _color;

  ItemDataCorte(this._title, this._data, this._color);

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    return Container(
        width: _size.width * 0.98,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(1),
          border: Border.all(width: 2.0, color: Colors.black38),
          color: Colors.white12,
        ),
        child: Column(children: <Widget>[
          SizedBox(
            height: 5,
          ),
          Text(
            _title,
            //  style: TextStyle(fontSize: 20),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            _data,
            style: TextStyle(fontSize: 20, color: _color),
          ),
          SizedBox(
            height: 5,
          ),
        ]));
  }
}
