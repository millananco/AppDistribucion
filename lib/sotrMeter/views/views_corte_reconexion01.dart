import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:distribucion/sotrMeter/models/models_corte_reconexion.dart';
import 'package:distribucion/sotrMeter/views/widgets/corte_reconexion01.dart';
import 'package:distribucion/sotrMeter/models/models_meter.dart';
import 'package:distribucion/presentation/layouts/layout_dinamic02.dart';
import 'package:distribucion/presentation/widgets/widget_index.dart';

class ViewCorteReconexion extends StatefulWidget {
  const ViewCorteReconexion({Key? key, this.meter}) : super(key: key);
  final ModelMeter? meter;
  @override
  // _HomePageState createState() => _HomePageState();
  State<ViewCorteReconexion> createState() => _HomePageState(meter!);
}

enum StatusWidget { inicio, hayDato, actualizaDato, prosesoDato, errorDato, timeout }

StatusWidget _statuswidged = StatusWidget.inicio;
int tsAnterior = 1;
int mystatus = 1;

class _HomePageState extends State<ViewCorteReconexion> {
  ModelMeter _meter;
  _HomePageState(this._meter); //constructor

  final textcontroller = TextEditingController();

  late DatabaseReference _dataCommand;
  late DatabaseReference _dataInstrumentation;
  late StreamSubscription<DatabaseEvent> _counterSubscription;
  FirebaseException? _error;
  // final Future<FirebaseApp> _future = Firebase.initializeApp();
  dynamic? data;
  bool hayData = false;
  bool timeout = false;

  @override
  void initState() {
    _init();

    super.initState();
  }

  Future<String> _getValue() async {
    timeout = false;
    await Future.delayed(Duration(seconds: 10));
    print('pasaron 10 segundos');
    setState(() {
      timeout = true;
    });

    return 'Woolha';
  }

  void _init() {
    String urlMyData = 'SOTR/IdDevice/' + _meter.tokenGateway! + '/Data/IdModbus/' + _meter.idModbus! + '/SetOpenClosed';
    _dataInstrumentation = FirebaseDatabase.instance.ref(urlMyData);

    _counterSubscription = _dataInstrumentation.onValue.listen(
      (DatabaseEvent event) {
        setState(() {
          _error = null;
          if (event.snapshot.value == null) {
            _getValue();
            print('Los datos son null');
          } else {
            print('Dato Ahora : ${event.snapshot.value}');
            //  mydata = '${event.snapshot.value}';
            data = event.snapshot.value;
            hayData = true;
            final String myData = data['Data'];
            final int ts = data['Ts'];
            print('MyData:' + myData);
            print('TS:' + ts.toString());
          }
        });
      },
      onError: (Object o) {
        final error = o as FirebaseException;
        setState(() {
          _error = error;
        });
      },
    );
    //  return mydata!;
    //   return data!;
  }

  void addDataOpen() {
    String urlComando = 'SOTR/IdDevice/' + _meter.tokenGateway! + '/Command';
    _dataCommand = FirebaseDatabase.instance.ref(urlComando);
    _dataCommand.set({
      //"{\"comando\":\"ModBus\",\"tipo\":\"SetOpenClosed\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":"+String(auxMeter)+",\"StateMeter\":\"Open\"}}"
      //"{\"comando\":\"ModBus\",\"tipo\":\"SetOpenClosed\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":"+String(auxMeter)+",\"StateMeter\":\"Close\"}}";
      'Data': "{\"comando\":\"ModBus\",\"tipo\":\"SetOpenClosed\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":\"" + _meter.idModbus! + "\",\"StateMeter\":\"Open\"}}", // data,
      'TS': ServerValue.timestamp
    });
    setState(() {
      mystatus = 2;
    });
  }

  void addDataClose() {
    String urlComando = 'SOTR/IdDevice/' + _meter.tokenGateway! + '/Command';
    _dataCommand = FirebaseDatabase.instance.ref(urlComando);
    _dataCommand.set({
      //"{\"comando\":\"ModBus\",\"tipo\":\"SetOpenClosed\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":"+String(auxMeter)+",\"StateMeter\":\"Open\"}}"
      //"{\"comando\":\"ModBus\",\"tipo\":\"SetOpenClosed\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":"+String(auxMeter)+",\"StateMeter\":\"Close\"}}";
      'Data': "{\"comando\":\"ModBus\",\"tipo\":\"SetOpenClosed\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":\"" + _meter.idModbus! + "\",\"StateMeter\":\"Close\"}}", // data,
      'TS': ServerValue.timestamp
    });
    setState(() {
      mystatus = 2;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget? myWidget;
    if (!timeout) {
      bool? progressindicator;
      List<Widget> children;
      if (hayData) {
        dynamic dato;
        dato = data;
        final String myData = dato['Data'];
        final int ts = dato['Ts'];
        print('MyData:' + myData);
        print('TS:' + ts.toString());
        final ModelCorteReconexion mydata = ModelCorteReconexion.fromJson(json.decode(myData));
        // final dynamic myenergy = json.decode(mydata);
        print('EstadoMedidor : ${mydata.statemeter}');
        final newYearsEve = DateTime.fromMillisecondsSinceEpoch(ts, isUtc: false);
        print(newYearsEve);

        if ((tsAnterior != ts) || (mystatus == 1)) {
          tsAnterior = ts;
          mystatus = 1;
          progressindicator = false;
        } else {
          progressindicator = true;
        }
        myWidget = WidgetCorteReconexion(lastSync: '${newYearsEve}', corteReconexion: mydata, btCorte: () => addDataOpen(), btReconexion: () => addDataClose());
      } else {
        print('Entro en:snapshot.hasError');
        myWidget = Center(child: Text('Error No hay datos...'));
        progressindicator = true;
      }

      return Scaffold(
          appBar: AppBar(
            title: Text("Corte Suministro"),
          ),
          body: LayoutDinamic(
            child: myWidget,
            progressindicator: progressindicator,
          ));
    } else {
      timeout = false;
      myWidget = Center(
          child: Column(
        children: <Widget>[
          SizedBox(
            height: 5,
          ),
          SizedBox(
            height: 10,
          ),
          RoundedButton(
            buttonName: 'Cortar Suministro',
            buttonAction: () => {
              addDataOpen()
            },
            radius: 1,
            /*  buttonAction:() => myhttpPost(context)*/
          ),
          SizedBox(
            height: 5,
          ),
          RoundedButton(
            buttonName: 'Reconectar Suministro',
            buttonAction: () => {
              addDataClose()
            },
            radius: 1,
            /*  buttonAction:() => myhttpPost(context)*/
          ),
        ],
      ));
      return Scaffold(
          appBar: AppBar(
            title: Text("Comando Suministro"),
          ),
          body: LayoutDinamic(
            child: myWidget,
            progressindicator: false,
          ));
    }
  }
}
