import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
//import 'package:distribucion/presentation/widgets/widget_list_meter.dart';
import 'package:distribucion/sotrMeter/views/widgets/widget_listMeter.dart';
import 'dart:core';
import 'package:distribucion/api/api-post03.dart';
import 'package:distribucion/sotrMeter/models/models_meter.dart';
import 'package:distribucion/api/model/model_index.dart';

class SotrMeter extends StatelessWidget {
  const SotrMeter({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final RequestPost requestPost = new RequestPost();

    final String myurl = "https://andronio.net/medidor/service/screen/Flutter01.php";
    final String mydato = '{"comando":"appandroid","tipo":"usuario","dato":{"funcion":"UsuarioMedidor"}}';
    // String myurl='https://jsonplaceholder.typicode.com/posts/1';
    return Center(
      child: MyAppP1(dataGet: requestPost.httpPost(myurl, mydato)),
      // child: MyApp(dataGet: httpGet(myurl)),
    );
  }
}

class MyAppP1 extends StatelessWidget {
  final Future<DataApi>? dataGet;
  //final Future<DataGet> dataGet;

  MyAppP1({Key? key, this.dataGet}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
          'Medidores',
        )),
        body: Container(
          child: FutureBuilder<DataApi>(
            future: dataGet,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return MedidorResponse(datos: snapshot.data!.dato); //Text(snapshot.data.status);
                //return Text('datos: ${snapshot.data!.dato}'); //Text(snapshot.data.status);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }

              // Por defecto, muestra un loading spinner
              return Center(child: CircularProgressIndicator());
            },
          ),
        ));
  }
}

class MedidorResponse extends StatelessWidget {
  final dynamic datos;

  MedidorResponse({this.datos});
  @override
  Widget build(BuildContext context) {
    List<ModelMeter> lismedidor;
    List medidores = datos['Medidor'];
    lismedidor = medidores.map((dynamic value) => new ModelMeter.fromJson(value[0])).toList();
    return Container(
      //  child: ListViewMedidor(mylist: lismedidor),
      child: ListMeter(listMeter: lismedidor),
    );
  }
}
/*
este es el json de la respuesta
{"comando":"usuario",
"tipo":"UsuarioMedidor",
"Status":"Req",
"Dato":{"Medidor":
[[{"Id":"3","tokenGateway":"95d9c56ab2bb26986049adde2e4323bca41df872","Lavel":"Informatica",
"IdLoraSlave":"0","IdModbus":"1","NumeroSerie":"282210148","IdTipoMedidor":"1","Suministro":"0",
"Estado":"1","FechaHora":"2021-03-10 12:01:40"}],
[{"Id":"2","tokenGateway":"ee394e83a6134e3e514f55e73ecb68d7e1f1294e","Lavel":"Laboratorio",
"IdLoraSlave":"0","IdModbus":"1","NumeroSerie":"01232912","IdTipoMedidor":"2","Suministro":"0",
"Estado":"1","FechaHora":"2021-01-13 09:57:08"}]]}}';
*/
