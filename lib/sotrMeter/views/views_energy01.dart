import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:distribucion/sotrMeter/models/models_energy.dart';
import 'package:distribucion/sotrMeter/models/models_meter.dart';
import 'package:distribucion/sotrMeter/views/widgets/energia01.dart';
//import 'package:distribucion/presentation/widgets/widget_index.dart';
import 'package:distribucion/presentation/layouts/layout_dinamic02.dart';

class ViewEnergy extends StatefulWidget {
  const ViewEnergy({Key? key, this.meter}) : super(key: key);
  final ModelMeter? meter;
  @override
  // _HomePageState createState() => _HomePageState();

  State<ViewEnergy> createState() => _HomePageState(meter!);
}

int tsAnterior = 1;
int mystatus = 1;

class _HomePageState extends State<ViewEnergy> {
  ModelMeter _meter;
  _HomePageState(this._meter); //constructor

  final textcontroller = TextEditingController();

  late DatabaseReference _dataCommand;
  late DatabaseReference _dataEnergy;
  late StreamSubscription<DatabaseEvent> _counterSubscription;
  FirebaseException? _error;

  // final Future<FirebaseApp> _future = Firebase.initializeApp();
  dynamic? data;
  bool hayData = false;
  @override
  void initState() {
    _init();

    super.initState();
  }

  Future<String> _getValue() async {
    await Future.delayed(Duration(seconds: 3));
    return 'Woolha';
  }

  void _init() {
    String urlEnergia = 'SOTR/IdDevice/' + _meter.tokenGateway! + '/Data/IdModbus/' + _meter.idModbus! + '/Energia';
    _dataEnergy = FirebaseDatabase.instance.ref(urlEnergia);

    _counterSubscription = _dataEnergy.onValue.listen(
      (DatabaseEvent event) {
        setState(() {
          _error = null;
          print('Dato Ahora : ${event.snapshot.value}');
          //    mydata = '${event.snapshot.value}';
          //  data = event.snapshot.value;
          hayData = true;
          data = event.snapshot.value;

          // final String energia = data['Data'];
          // final int ts = data['Ts'];
          // print('Energia:' + energia);
          //print('TS:' + ts.toString());
        });
      },
      onError: (Object o) {
        final error = o as FirebaseException;
        print('Error datos hoy');
        setState(() {
          _error = error;
        });
      },
    );
  }

  void _addData() {
    print('Ejecuta funtion');
    String urlComando = 'SOTR/IdDevice/' + _meter.tokenGateway! + '/Command';
    _dataCommand = FirebaseDatabase.instance.ref(urlComando);
    _dataCommand.set({
      'Data': "{\"comando\":\"ModBus\",\"tipo\":\"Energia\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":" + _meter.idModbus! + "}}", // data,
      'TS': ServerValue.timestamp
    });
    setState(() {
      mystatus = 2;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool? progressindicator;
    Widget? myWidget;
    //List<Widget>? children;

    if (hayData) {
      print('Entroen:snapshot.hasData');
      dynamic? dato;
      dato = data;
      final String energia = dato['Data'];
      final int ts = dato['Ts'];

      print('Energia:' + energia);
      print('TS:' + ts.toString());
      final ModelEnergy myenergy = ModelEnergy.fromJson(json.decode(energia));
      // final dynamic myenergy = json.decode(energia1);
      final newYearsEve = DateTime.fromMillisecondsSinceEpoch(ts, isUtc: false);
      print(newYearsEve);

      print('demandaMaxima : ${myenergy.demandMaxVA}');

      if ((tsAnterior != ts) || (mystatus == 1)) {
        tsAnterior = ts;
        mystatus = 1;
        progressindicator = false;
      } else {
        progressindicator = true;
      }
      myWidget = WidgetEnergia(lastSync: '${newYearsEve}', myenergy: myenergy, btActualizar: () => _addData());
    } else {
      print('Entro en:snapshot.hasError');
      myWidget = Center(child: Text('Error No hay datos...'));
      progressindicator = true;
    }

    return Scaffold(
        appBar: AppBar(
          title: Text("Energia"),
        ),
        body: LayoutDinamic(
          child: myWidget,
          progressindicator: progressindicator,
        ));
  }
}
