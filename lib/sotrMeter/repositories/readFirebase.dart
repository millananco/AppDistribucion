//import 'package:http/http.dart' as http;
import 'dart:core';
import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';

class ReadFirebase {
  Future<dynamic> readFirebase(String url, String dato) async {
    late dynamic mydata;
    late DatabaseReference _counterRef;
    late StreamSubscription<DatabaseEvent> _counterSubscription;
    FirebaseException? _error;

    _counterRef = FirebaseDatabase.instance.ref('SOTR/IdDevice/ee394e83a6134e3e514f55e73ecb68d7e1f1294e/Data/IdModbus/1/Energia');
    _counterSubscription = _counterRef.onValue.listen(
      (DatabaseEvent event) {
        //   {
        _error = null;
        print('Event Type: ${event.type}'); // DatabaseEventType.value;
        print('Snapshot: ${event.snapshot}'); // DataSnapshot
        print('Data : ${event.snapshot.value}');
        mydata = '${event.snapshot.value}';
        return (mydata);
        // }
      },
      onError: (Object o) {
        final error = o as FirebaseException;
        {
          _error = error;
        }
        return ("ErrorFirebase");
      },
    );
    // return("Error");
  }
}
