import 'dart:async';
import 'dart:convert';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:distribucion/sotrMeter/repositories/sotrMeter_repocitories.dart';
import 'package:distribucion/sotrMeter/models/models_energy.dart';

class SotrMeterRepositoryImpl implements SotrMeterRepository {
  late DatabaseReference _counterRef;
  late StreamSubscription<DatabaseEvent> _counterSubscription;
  FirebaseException? _error;
  @override
  Future<ModelEnergy> getEnergy() async {
    late ModelEnergy mydata;
    _counterRef = FirebaseDatabase.instance.ref('SOTR/IdDevice/ee394e83a6134e3e514f55e73ecb68d7e1f1294e/Data/IdModbus/1/Energia');

    _counterSubscription = _counterRef.onValue.listen(
      (DatabaseEvent event) {
        {
          _error = null;
          print('Event Type: ${event.type}'); // DatabaseEventType.value;
          print('Snapshot: ${event.snapshot}'); // DataSnapshot
          print('Data : ${event.snapshot.value}');
          mydata = ModelEnergy.fromJson(json.decode('${event.snapshot.value}'));
        }
      },
      onError: (Object o) {
        final error = o as FirebaseException;
        {
          _error = error;
        }
      },
    );
    return (mydata);
  }
}
