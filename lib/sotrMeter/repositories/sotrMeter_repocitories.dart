import 'package:distribucion/sotrMeter/models/models_energy.dart';

abstract class SotrMeterRepository {
  Future<ModelEnergy> getEnergy();
}
