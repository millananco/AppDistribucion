class ModelCorteReconexion {
  String? id;
  String? statusl1;
  String? statusl2;
  String? statusl3;
  String? statemeter;

  ModelCorteReconexion({
    this.id,
    this.statusl1,
    this.statusl2,
    this.statusl3,
    this.statemeter,
  });

  factory ModelCorteReconexion.fromJson(Map<String, dynamic> json) {
    return ModelCorteReconexion(
      id: json['IdDireccion'],
      statusl1: json['StatusOutput'][0],
      statusl2: json['StatusOutput'][1],
      statusl3: json['StatusOutput'][2],
      statemeter: json['StateMeter'],
    );
  }
}
