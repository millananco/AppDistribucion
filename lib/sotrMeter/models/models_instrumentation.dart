class ModelInstrumentation {
  int? id;
  String? vl12;
  String? vl23;
  String? vl31;
  String? il1;
  String? il2;
  String? il3;
  String? cosql1;
  String? cosql2;
  String? cosql3;
  ModelInstrumentation({
    this.id,
    this.vl12,
    this.vl23,
    this.vl31,
    this.il1,
    this.il2,
    this.il3,
    this.cosql1,
    this.cosql2,
    this.cosql3,
  });

  factory ModelInstrumentation.fromJson(Map<String, dynamic> json) {
    return ModelInstrumentation(
      id: json['IdDireccion'],
      vl12: json['VL12'],
      vl23: json['VL23'],
      vl31: json['VL31'],
      il1: json['IL1'],
      il2: json['IL2'],
      il3: json['IL3'],
      cosql1: json['CosQL1'],
      cosql2: json['CosQL2'],
      cosql3: json['CosQL3'],
    );
  }
}
