class ModelEnergy {
  int? id;
  String? energyImport;
  String? energyExport;
  String? reactiveImport;
  String? reactiveExport;
  String? demandMaxWatts;
  String? demandMaxVA;
  String? voltAh;
  ModelEnergy({
    this.id,
    this.energyImport,
    this.energyExport,
    this.reactiveImport,
    this.reactiveExport,
    this.demandMaxWatts,
    this.demandMaxVA,
    this.voltAh,
  });

  factory ModelEnergy.fromJson(Map<String, dynamic> json) {
    return ModelEnergy(
      id: json['IdDireccion'],
      energyImport: json['Energia']['Import'],
      energyExport: json['Energia']['Export'],
      reactiveImport: json['Reactiva']['Import'],
      reactiveExport: json['Reactiva']['Export'],
      demandMaxWatts: json['Demanda']['MaximaWatts'],
      demandMaxVA: json['Demanda']['MaximaVA'],
      voltAh: json['VoltAmperHora'],
    );
  }
}
