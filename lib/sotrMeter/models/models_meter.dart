class ModelMeter {
  String? id;
  String? tokenGateway;
  String? lavel;
  String? idLoraSlave;
  String? idModbus;
  String? numeroSerie;
  String? idTipoMedidor;
  String? suministro;
  String? estado;
  String? fechaHora;
  ModelMeter({
    this.id,
    this.tokenGateway,
    this.lavel,
    this.idLoraSlave,
    this.idModbus,
    this.numeroSerie,
    this.idTipoMedidor,
    this.suministro,
    this.estado,
    this.fechaHora,
  });

  factory ModelMeter.fromJson(Map<String, dynamic> json) {
    return ModelMeter(
      id: json['Id'],
      tokenGateway: json['tokenGateway'],
      lavel: json['Lavel'],
      idLoraSlave: json['IdLoraSlave'],
      idModbus: json['IdModbus'],
      numeroSerie: json['NumeroSerie'],
      idTipoMedidor: json['IdTipoMedidor'],
      suministro: json['Suministro'],
      estado: json['Estado'],
      fechaHora: json['FechaHora'],
    );
  }
}
