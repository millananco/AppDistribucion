part of 'SotrMeter_bloc.dart';

abstract class SotrMeterEvent extends Equatable {}

class LoadSotrMeter extends SotrMeterEvent {
  @override
  List<Object> get props => [];
}

class UpdateSotrMeter extends SotrMeterEvent {
  List<Object> get props => [];
}

class OpenMenuSotrMeter extends SotrMeterEvent {
  List<Object> get props => [];
}

class CloseMenuSotrMeter extends SotrMeterEvent {
  List<Object> get props => [];
}

class InicioAddSotrMeter extends SotrMeterEvent {
  List<Object> get props => [];
}

class FoundSotrMeter extends SotrMeterEvent {
  List<Object> get props => [];
}
