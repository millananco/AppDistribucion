import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
//import 'package:coopeluzplottier/models/theme.dart';

part 'SotrMeter_event.dart';
part 'SotrMeter_state.dart';

class SotrMeterBloc extends Bloc<SotrMeterEvent, SotrMeterState> {
  SotrMeterBloc() : super(InitialSotrMeterState());

  @override
  Stream<SotrMeterState> mapEventToState(SotrMeterEvent event) async* {
    if (event is OpenMenuSotrMeter) yield SotrMeterOnMenu();
    if (event is CloseMenuSotrMeter) yield SotrMeterOffMenu();
    //if (event is FoundSotrMeter) yield WatchSotrMeterState();
  }
}
