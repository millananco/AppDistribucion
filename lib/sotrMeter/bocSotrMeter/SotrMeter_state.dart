part of 'SotrMeter_bloc.dart';

abstract class SotrMeterState extends Equatable {}

class InitialSotrMeterState extends SotrMeterState {
  @override
  List<Object> get props => [];
}

class SotrMeterNotLoaded extends SotrMeterState {
  @override
  List<Object> get props => [];
}

class SotrMeterTipo extends SotrMeterState {
  // final Theme theme;

  // SuministroTipo({@required this.theme});

  @override
  List<Object> get props => [];
}

class SotrMeterOnMenu extends SotrMeterState {
  @override
  List<Object> get props => [];
}

class SotrMeterOffMenu extends SotrMeterState {
  @override
  List<Object> get props => [];
}

class AddSotrMetertate extends SotrMeterState {
  @override
  List<Object> get props => [];
}

class SearchSotrMeterState extends SotrMeterState {
  @override
  List<Object> get props => [];
}
