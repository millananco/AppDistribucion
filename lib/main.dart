import 'package:distribucion/authentication_repository/authentication_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/widgets.dart';
import 'package:distribucion/app/app.dart';

import 'package:distribucion/notificacion/firebase_config.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:flutter/foundation.dart';

import 'package:distribucion/theme/repositories/preferences_repository_impl.dart';
import 'package:distribucion/theme/bloc/preferences_bloc.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  // await Firebase.initializeApp(options: DefaultFirebaseConfig.platformOptions);
  print('Handling a background message ${message.messageId}');
}

/// Create a [AndroidNotificationChannel] for heads up notifications
late AndroidNotificationChannel channel;

/// Initialize the [FlutterLocalNotificationsPlugin] package.
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

Future<void> main() async {
  Bloc.observer = AppBlocObserver();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      /*  options: const FirebaseOptions(
      appId: '1:554055765152:android:391c259036076645650dc5',
      apiKey: 'AIzaSyCgMEl8YpgV0s932fHHFKphmpTmQkPeUoY',
      projectId: 'miclan1503',
      messagingSenderId: '554055765152',
      authDomain: 'miclan1503.firebaseapp.com',
      androidClientId: '554055765152-k65pe5bces7q7oe23lb71so0c69mm6ot.apps.googleusercontent.com',
    ),*/
      );

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  if (!kIsWeb) {
    channel = const AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      description: 'This channel is used for important notifications.', // description
      importance: Importance.high,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    /// Create an Android Notification Channel.
    ///
    /// We use this channel in the `AndroidManifest.xml` file to override the
    /// default FCM channel to enable heads up notifications.
    await flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()?.createNotificationChannel(channel);

    /// Update the iOS foreground notification presentation options to allow
    /// heads up notifications.
    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  final authenticationRepository = AuthenticationRepository();
  await authenticationRepository.user.first;

  final preferencesRepository = PreferencesRepositoryImpl();
  final preferencesBloc = PreferencesBloc(preferencesRepository: preferencesRepository);

  runApp(App(preferencesRepository: preferencesRepository, preferencesBloc: preferencesBloc, authenticationRepository: authenticationRepository));
  preferencesBloc.add(LoadPreferences());
}
