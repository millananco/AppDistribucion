import 'package:distribucion/authentication_repository/authentication_repository.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:flutter/material.dart' hide Theme;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:distribucion/app/app.dart';
//import 'package:distribucion/theme.dart';
import 'package:distribucion/theme/repositories/preferences_repository.dart';
import 'package:distribucion/theme/bloc/preferences_bloc.dart';
import 'package:distribucion/presentation/styles/themes_data.dart';
import 'package:distribucion/theme/models/theme.dart';

class App extends StatelessWidget {
  const App({
    Key? key,
    required PreferencesRepository preferencesRepository,
    required PreferencesBloc preferencesBloc,
    required AuthenticationRepository authenticationRepository,
  })   : _authenticationRepository = authenticationRepository,
        _preferencesRepository = preferencesRepository,
        _preferencesBloc = preferencesBloc,
        super(key: key);

  final AuthenticationRepository _authenticationRepository;
  final PreferencesRepository _preferencesRepository;
  final PreferencesBloc _preferencesBloc;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<PreferencesRepository>.value(
          value: _preferencesRepository,
        ),
        RepositoryProvider<AuthenticationRepository>.value(
          value: _authenticationRepository,
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<PreferencesBloc>.value(value: _preferencesBloc),
          //       BlocProvider<SuministroBloc>(create: (context) => SuministroBloc()),
          BlocProvider<AppBloc>(
              create: (_) => AppBloc(
                    authenticationRepository: _authenticationRepository,
                  ))
        ],
        child: BlocBuilder<PreferencesBloc, PreferencesState>(
          builder: (context, state) {
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              //title: "Myapp",
              theme: state is PreferencesLoaded ? themesData[state.theme] : themesData[Theme.light], //themesData[Theme.mylight],
              home: FlowBuilder<AppStatus>(
                state: context.select((AppBloc bloc) => bloc.state.status),
                onGeneratePages: onGenerateAppViewPages,
              ),
              /*initialRoute: Login.routeName, //HomeScreen.routeName,
              routes: {
                HomeScreen.routeName: (context) => HomeScreen(),
                CounterWithLocalStateScreen.routeName: (context) => CounterWithLocalStateScreen(),
                CounterWithGlobalStateScreen.routeName: (context) => CounterWithGlobalStateScreen(),
                StopwatchWithLocalStateScreen.routeName: (context) => StopwatchWithLocalStateScreen(),
                StopwatchWithGlobalStateScreen.routeName: (context) => StopwatchWithGlobalStateScreen(),
                ThemeSelectorScreen.routeName: (context) => ThemeSelectorScreen(),
                Login.routeName: (context) => Login(),
                Registrar.routeName: (context) => Registrar(),
                RecuperarPassword.routeName: (context) => RecuperarPassword(),
                Suministro.routeName: (context) => Suministro(),
                AddSuministro.routeName: (context) => AddSuministro(),
                VerFacturas.routeName: (context) => VerFacturas(),
              },*/
            );
          },
        ),
      ),
    );
  }
}
/*
    return RepositoryProvider.value(
      value: _authenticationRepository,
      child: BlocProvider(
        create: (_) => AppBloc(
          authenticationRepository: _authenticationRepository,
        ),
        child: const AppView(),
      ),
    );
  }
}

class AppView extends StatelessWidget {
  const AppView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: theme,
      home: FlowBuilder<AppStatus>(
        state: context.select((AppBloc bloc) => bloc.state.status),
        onGeneratePages: onGenerateAppViewPages,
      ),
    );
  }
}
*/
