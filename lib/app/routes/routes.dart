import 'package:flutter/widgets.dart';
import 'package:distribucion/app/app.dart';
import 'package:distribucion/home/home.dart';
import 'package:distribucion/login/login.dart';

List<Page> onGenerateAppViewPages(AppStatus state, List<Page<dynamic>> pages) {
  switch (state) {
    case AppStatus.authenticated:
      return [
        HomePage.page()
      ];
    case AppStatus.unauthenticated:
    default:
      return [
        LoginPage.page()
      ];
  }
}
