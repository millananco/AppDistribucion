import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';

class DefaultFirebaseConfig {
  static FirebaseOptions get platformOptions {
    if (kIsWeb) {
      // Web
      return const FirebaseOptions(
        apiKey: 'AIzaSyCCjxv-lG1vwe26jxN4r2GGDZxxfG8eZEw',
        authDomain: 'myapp-9d5ca.firebaseapp.com',
        databaseURL: 'https://myapp-9d5ca.firebaseio.com',
        projectId: 'myapp-9d5ca',
        storageBucket: 'myapp-9d5ca.appspot.com',
        messagingSenderId: '505516512406',
        appId: '1:505516512406:web:0962746f10fd3929ca9eb7',
        measurementId: 'G-20E5SPESP',
      );
    } else if (Platform.isIOS || Platform.isMacOS) {
      // iOS and MacOS
      return const FirebaseOptions(
        apiKey: 'AIzaSyAHAsf51D0A407EklG1bs-5wA7EbyfNFg0',
        appId: '1:448618578101:ios:0b11ed8263232715ac3efc',
        messagingSenderId: '448618578101',
        projectId: 'miclan1503',
        authDomain: 'miclan1503.firebaseapp.com',
        iosBundleId: 'io.flutter.plugins.firebase.messaging',
        iosClientId: '448618578101-evbjdqq9co9v29pi8jcua8bm7kr4smuu.apps.googleusercontent.com',
        databaseURL: 'https://miclan1503.firebaseio.com',
      );
    } else {
      // Android
      return const FirebaseOptions(
        appId: '1:505516512406:android:943b75e42d9b7f87ca9eb7',
        apiKey: 'AIzaSyD9axgeith-gPEVeIetVELsyQIdgqdinKk',
        projectId: 'myapp-9d5ca',
        messagingSenderId: '505516512406',
        authDomain: 'myapp-9d5ca.firebaseapp.com',
        androidClientId: '505516512406-foqilbd8peve3lm1g8kcbh8e36ap1dct.apps.googleusercontent.com',
        databaseURL: 'https://myapp-9d5ca.firebaseio.com',
      );
    }
  }
}
