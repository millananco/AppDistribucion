import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';

class DefaultFirebaseConfig {
  static FirebaseOptions get platformOptions {
    if (kIsWeb) {
      // Web
      return const FirebaseOptions(
        apiKey: 'AIzaSyA1mgo1XL0YSEBlkbL0oSOj-PfnAB2DDMg',
        authDomain: 'miclan1503.firebaseapp.com',
        databaseURL: 'https://miclan1503.firebaseio.com',
        projectId: 'miclan1503',
        storageBucket: 'miclan1503.appspot.com',
        messagingSenderId: '554055765152',
        appId: '1:554055765152:web:a2d36c41d9d414cd650dc5',
        measurementId: 'G-20E5SPESP',
      );
    } else if (Platform.isIOS || Platform.isMacOS) {
      // iOS and MacOS
      return const FirebaseOptions(
        apiKey: 'AIzaSyAHAsf51D0A407EklG1bs-5wA7EbyfNFg0',
        appId: '1:448618578101:ios:0b11ed8263232715ac3efc',
        messagingSenderId: '448618578101',
        projectId: 'miclan1503',
        authDomain: 'miclan1503.firebaseapp.com',
        iosBundleId: 'io.flutter.plugins.firebase.messaging',
        iosClientId: '448618578101-evbjdqq9co9v29pi8jcua8bm7kr4smuu.apps.googleusercontent.com',
        databaseURL: 'https://miclan1503.firebaseio.com',
      );
    } else {
      // Android
      return const FirebaseOptions(
        appId: '1:554055765152:android:391c259036076645650dc5',
        apiKey: 'AIzaSyCgMEl8YpgV0s932fHHFKphmpTmQkPeUoY',
        projectId: 'miclan1503',
        messagingSenderId: '554055765152',
        authDomain: 'miclan1503.firebaseapp.com',
        androidClientId: '554055765152-k65pe5bces7q7oe23lb71so0c69mm6ot.apps.googleusercontent.com',
      );
    }
  }
}
