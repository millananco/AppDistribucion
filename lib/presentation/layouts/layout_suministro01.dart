import 'package:flutter/material.dart' hide Theme;
import 'package:distribucion/theme/bloc/preferences_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:distribucion/theme/models/theme.dart';

class LayoutSuministro extends StatelessWidget {
  final Widget child;
  final Widget menu;
  final Widget blackground;
  final bool progressindicator;
  final bool viewmenu;
  final double opacity;
  final Color color;
  LayoutSuministro({
    Key? key,
    required this.child,
    required this.menu,
    required this.viewmenu,
    required this.progressindicator,
    required this.blackground,
    this.opacity = 0.5,
    this.color = Colors.grey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> widgetList = <Widget>[];

    return BlocBuilder<PreferencesBloc, PreferencesState>(builder: (context, state) {
      if (state is PreferencesLoaded) {
        widgetList.add(child);
        /*
       if ((state.theme == Theme.mylight) || (state.theme == Theme.mydark)) {
          widgetList.add(blackground);
          widgetList.add(child);
        } else {
          widgetList.add(child);
        }
     */
      }
      if (viewmenu) {
        widgetList.add(Opacity(
          opacity: opacity,
          child: ModalBarrier(dismissible: false, color: color),
        ));
        widgetList.add(menu);
      }
      if (progressindicator) {
        widgetList.add(Opacity(
          opacity: opacity,
          child: ModalBarrier(dismissible: false, color: color),
        ));
        widgetList.add(Center(child: new CircularProgressIndicator()));
      }

      return Stack(children: widgetList);
    });
  }
}
