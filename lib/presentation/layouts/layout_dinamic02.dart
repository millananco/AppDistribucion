import 'package:flutter/material.dart' hide Theme;

class LayoutDinamic extends StatelessWidget {
  final Widget child;
  //final Widget blackground;
  final bool progressindicator;
  final double opacity;
  final Color color;
  LayoutDinamic({
    Key? key,
    required this.child,
    required this.progressindicator,
    //  required this.blackground,
    this.opacity = 0.4,
    this.color = Colors.grey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> widgetList = <Widget>[]; //new List<Widget>.empty();

    widgetList.add(child);

    if (progressindicator) {
      widgetList.add(Opacity(
        opacity: opacity,
        child: ModalBarrier(dismissible: false, color: color),
      ));
      widgetList.add(Center(child: new CircularProgressIndicator()));
    }

    return Stack(children: widgetList);
  }
}
