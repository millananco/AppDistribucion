import 'package:flutter/material.dart';
//import 'dart:core';
import 'package:distribucion/presentation/styles/themes_data.dart';
import 'package:flutter/cupertino.dart';
//import 'package:flutter/src/widgets/basic.dart';

class ListSuminstro extends StatelessWidget {
  ListSuminstro({Key? key, required this.mylist, this.width}) : super(key: key);

  final List<EntitySumintro> mylist;
  final double? width;
  @override
  Widget build(BuildContext context) {
    //   Size size = MediaQuery.of(context).size;
    return Container(
      color: kBlue,
      width: width,
      child: ListView.builder(
        primary: false,
        itemBuilder: (BuildContext context, int index) => new ListItemWidget(mylist[index]),
        itemCount: mylist.length,
        shrinkWrap: true,
      ),
    );
  }
}

class ListItemWidget extends StatelessWidget {
  final EntitySumintro? _itmEntidadSuministro;
  // final String _title;
  // final String _data;
  ListItemWidget(this._itmEntidadSuministro);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        _itmEntidadSuministro!.etiqueta!,
        //title: Text(_title,
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w700,
        ),
      ),
      subtitle: SingleChildScrollView(
        child: Text(_itmEntidadSuministro!.suministro!),
      ),
      onTap: () {},
    );
  }
}

class EntitySumintro {
  String? etiqueta;
  String? suministro;
  String? tipo;
  EntitySumintro({this.etiqueta, this.suministro, this.tipo});

  factory EntitySumintro.fromJson(Map<String, dynamic> json) => EntitySumintro(
        etiqueta: json["etiqueta"],
        suministro: json["suministro"],
        tipo: json["tipo"],
      );
}
