import 'package:flutter/material.dart';
//import 'dart:core';
import 'package:flutter/cupertino.dart';
//import 'package:flutter/src/widgets/basic.dart';

class ListViewScroll extends StatelessWidget {
  ListViewScroll({
    Key? key,
    required this.mylist,
  }) : super(key: key);

  final List<ListEntity> mylist;

  @override
  Widget build(BuildContext context) {
    //   Size size = MediaQuery.of(context).size;
    return Center(
      child: ListView.builder(
        primary: false,
        itemBuilder: (BuildContext context, int index) => new ListItemWidget(mylist[index].title!, mylist[index].data!),
        itemCount: mylist.length,
        shrinkWrap: true,
      ),
    );
  }
}

class ListItemWidget extends StatelessWidget {
  final String _title;
  final String _data;
  ListItemWidget(this._title, this._data);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        _title,
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w700,
        ),
      ),
      subtitle: SingleChildScrollView(
        child: Text(_data),
      ),
      onTap: () {},
    );
  }
}

class ListEntity {
  String? title;
  String? data;
  ListEntity({this.title, this.data});

  factory ListEntity.fromJson(Map<String, dynamic> json) => ListEntity(
        title: json["title"],
        data: json["data"],
      );
}
