import 'package:flutter/material.dart';
import 'package:distribucion/presentation/styles/themes_data.dart';
import 'dart:core';

class RoundedButton extends StatelessWidget {
  const RoundedButton({
    Key? key,
    required this.buttonName,
    required this.buttonAction,
    this.radius,
  }) : super(key: key);

  final String buttonName;
  final void Function()? buttonAction;
  final double? radius;
  @override
  Widget build(BuildContext context) {
    double radio;
    if (radius == null)
      radio = 16;
    else
      radio = radius!;
    Size size = MediaQuery.of(context).size;
    return Container(
      // height: size.height * 0.08,
      width: size.width * 0.7,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radio),
        //   color: kBlue,
      ),
      child: FlatButton(
        onPressed: buttonAction,
        child: Text(
          buttonName,
          style: const TextStyle(color: kBlue),
          // style: kBodyText.copyWith(fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
