import 'package:flutter/material.dart';

class GradienteLineal extends StatelessWidget {
  const GradienteLineal({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: <Color>[
            //Color(0xffee0000),Color(0xffeeee00)
            //Colors.yellow,
            //  Color(0xfffff9c4),
            Color(0xffa5d6a7),
            Color(0xff81c784),
            Colors.green,

            //  kBlue,
          ], // red to yellow
          tileMode: TileMode.clamp, // repeats the gradient over the canvas
        ),
      ),
    );
  }
}
