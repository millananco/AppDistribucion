import 'package:flutter/material.dart';
import 'dart:core';

class MyZisedBar extends StatelessWidget {
  const MyZisedBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.height * 0.07,
    );
  }
}
