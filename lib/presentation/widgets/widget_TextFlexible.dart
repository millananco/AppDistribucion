import 'package:flutter/material.dart';
import 'package:distribucion/presentation/styles/themes_data.dart';
import 'dart:core';

class TextFlexible extends StatelessWidget {
  const TextFlexible({
    Key? key,
    // @required this.icon,
    required this.text,
  }) : super(key: key);

  // final IconData icon;
  final String text;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return /*Padding(
      padding: const EdgeInsets.symmetric(vertical: 1.0),
      child: */
        Container(
      // height: size.height * 0.08,
      width: size.width * 0.8,
      decoration: BoxDecoration(
        color: Colors.grey.shade500.withOpacity(0.5),
        borderRadius: BorderRadius.circular(1),
      ),
      child: Center(
        child: Text(
          text,
          style: kBodyText,
        ),
      ),
      //),
    );
  }
}
