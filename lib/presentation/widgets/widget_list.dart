import 'package:flutter/material.dart';
//import 'dart:core';
import 'package:flutter/cupertino.dart';
//import 'package:flutter/src/widgets/basic.dart';
//import 'package:distribucion/sotrMeter/views/views_energia.dart';

class ListViewScroll extends StatelessWidget {
  ListViewScroll({
    Key? key,
    required this.mylist,
  }) : super(key: key);

  final List<ListEntity> mylist;

  @override
  Widget build(BuildContext context) {
    //   Size size = MediaQuery.of(context).size;
    return Center(
      child: ListView.builder(
        primary: false,
        itemBuilder: (BuildContext context, int index) => new ListItemWidget(mylist[index].icon!, mylist[index].title!, mylist[index].data!, mylist[index].functionAction!),
        itemCount: mylist.length,
        shrinkWrap: true,
      ),
    );
  }
}

class ListItemWidget extends StatelessWidget {
  final IconData _icon;
  final String _title;
  final String _data;
  final Widget _functionAction;
  ListItemWidget(this._icon, this._title, this._data, this._functionAction);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(leading: Icon(_icon, size: 50.0), title: Text(_title), subtitle: Text(_data), trailing: Icon(Icons.more_vert), onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => _functionAction))
          // () =>
          //    _functionAction

          ),
    );
  }
}

class ListEntity {
  final IconData? icon;
  String? title;
  String? data;
  Widget? functionAction;
  ListEntity({this.icon, this.title, this.data, this.functionAction});

  factory ListEntity.fromJson(Map<String, dynamic> json) => ListEntity(
        icon: json["icon"],
        title: json["title"],
        data: json["data"],
        functionAction: json["functionAction"],
      );
}
