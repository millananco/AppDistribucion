import 'package:flutter/material.dart';
import 'package:distribucion/sotrMeter/models/models_meter.dart';
import 'package:distribucion/sotrMeter/views/widgets/Medidor01.dart';

class ListViewMedidor extends StatelessWidget {
  ListViewMedidor({
    Key? key,
    required this.mylist,
  }) : super(key: key);

  final List<ModelMeter> mylist;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      primary: false,
      itemBuilder: (BuildContext context, int index) => new ListItemMedidor(mylist[index].lavel!, mylist[index].idModbus!, mylist[index].numeroSerie!, mylist[index].estado!, mylist[index].tokenGateway!),
      itemCount: mylist.length,
      shrinkWrap: true,
    );
  }
}

class ListItemMedidor extends StatelessWidget {
  final String _lavel;
  final String _idModbus;
  final String _numeroSerie;
  final String _estado;
  final String _tokenGateway;
  ListItemMedidor(this._lavel, this._idModbus, this._numeroSerie, this._estado, this._tokenGateway);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        _lavel,
        style: TextStyle(
          fontSize: 22,
          fontWeight: FontWeight.w200,
        ),
      ),
      subtitle: SingleChildScrollView(
        child: Text("IdModbus:" + _idModbus + "\r\n" + "NS:" + _numeroSerie + "\r\n" + "Estado:" + _estado,
            style: TextStyle(
              fontSize: 20,
            )),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Medidor01(lavel: _lavel, idModbus: _idModbus, numeroSerie: _numeroSerie, estado: _estado, tokenGateway: _tokenGateway)),
        );
      },
    );
  }
}
