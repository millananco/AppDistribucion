import 'package:flutter/material.dart' hide Theme;
import 'package:distribucion/theme/models/theme.dart';

final themesData = {
  // Theme.light: ThemeData(
  Theme.light: ThemeData(
    // Define the default brightness and colors.
    // brightness: Brightness.light,
    brightness: Brightness.light,
    //primaryColor: Colors.lightBlue[800],
    //accentColor: Colors.cyan[600],

    // Define the default font family.
    fontFamily: 'Arial',

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    /*
    textTheme: const TextTheme(
      headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
      headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
      bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
    ),*/
  ),
  Theme.dark: ThemeData(
    // brightness: Brightness.dark,
    // Define the default brightness and colors.
    // brightness: Brightness.light,
    brightness: Brightness.dark,
    //primaryColor: Colors.lightBlue[800],
    //accentColor: Colors.cyan[600],

    // Define the default font family.
    fontFamily: 'Arial',
  ),
  /*
  Theme.mylight: ThemeData(
    // Define the default brightness and colors.
    // brightness: Brightness.light,
    brightness: Brightness.light,
    primaryColor: Colors.lightBlue[800],
    accentColor: Colors.cyan[600],

    // Define the default font family.
    fontFamily: 'Arial',
  ),
  Theme.mydark: ThemeData(
    // brightness: Brightness.dark,
    // Define the default brightness and colors.
    // brightness: Brightness.light,
    brightness: Brightness.dark,
    primaryColor: Colors.lightBlue[800],
    accentColor: Colors.cyan[600],

    // Define the default font family.
    fontFamily: 'Arial',
  ),
*/
};

const TextStyle kBodyText = TextStyle(fontSize: 22, height: 1.5);
const TextStyle kMenuText = TextStyle(fontSize: 22, height: 1.5);

//const TextStyle kMenuText = TextStyle(fontSize: 22, color: Colors.black, height: 1.5);
const Color kWhite = Colors.white;
const Color myBlue = Colors.blue;
const Color kBlue = Color(0xff00ACC1); // Color(0xff5663ff);
