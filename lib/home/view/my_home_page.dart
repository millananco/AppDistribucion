import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:distribucion/sotrMeter/models/models_energy.dart';

class MyHomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<MyHomePage> {
  final textcontroller = TextEditingController();
  final databaseRef = FirebaseDatabase.instance.ref('SOTR/IdDevice/ee394e83a6134e3e514f55e73ecb68d7e1f1294e/Command');
  late DatabaseReference _counterRef;
  late StreamSubscription<DatabaseEvent> _counterSubscription;
  FirebaseException? _error;
  final Future<FirebaseApp> _future = Firebase.initializeApp();

  @override
  void initState() {
    //init();
    super.initState();
  }

  Future<String> _getValue() async {
    await Future.delayed(Duration(seconds: 3));
    return 'Woolha';
  }

  /*Future<void> init() async {
    _counterRef = FirebaseDatabase.instance.ref('SOTR/IdDevice/ee394e83a6134e3e514f55e73ecb68d7e1f1294e/Data/IdModbus/1/Energia');

    _counterSubscription = _counterRef.onValue.listen(
      (DatabaseEvent event) {
        setState(() {
          _error = null;
          print('Event Type: ${event.type}'); // DatabaseEventType.value;
          print('Snapshot: ${event.snapshot}'); // DataSnapshot
          print('Data : ${event.snapshot.value}');
        });
      },
      onError: (Object o) {
        final error = o as FirebaseException;
        setState(() {
          _error = error;
        });
      },
    );
  }
*/
  Future<String> _init() async {
    String? mydata;
    dynamic? data;
    // ModelEnergy? mydata;

    _counterRef = FirebaseDatabase.instance.ref('SOTR/IdDevice/ee394e83a6134e3e514f55e73ecb68d7e1f1294e/Data/IdModbus/1/Energia');

    _counterSubscription = _counterRef.onValue.listen(
      (DatabaseEvent event) {
        setState(() {
          _error = null;
          //  print('Event Type: ${event.type}'); // DatabaseEventType.value;
          //  print('Snapshot: ${event.snapshot}'); // DataSnapshot
          print('Dato Ahora : ${event.snapshot.value}');
          mydata = '${event.snapshot.value}';
          data = event.snapshot.value;
          final String energia = data['Data'];
          final int ts = data['Ts'];
          print('Energia:' + energia);
          print('TS:' + ts.toString());
          final String energia1 = '{"IdDireccion":1,"Energia":{"Import":"0.13kWh","Export":"0.01kWh"},"Reactiva":{"Import":"0.01kVArh","Export":"2.08kVArh"},"Demanda":{"MaximaWatts":"-0.00Watts","MaximaVA":"-0.03VA"},"VoltAmperHora":"2.09kVAh","Result":"Ok"}';
          final ModelEnergy myenergy = ModelEnergy.fromJson(json.decode(energia1));
          // final dynamic myenergy = json.decode(energia1);
          print('Array : ${myenergy.demandMaxVA}');
        });
        //   });
      },
      onError: (Object o) {
        final error = o as FirebaseException;
        setState(() {
          _error = error;
        });
      },
    );
    //return data!;
    return mydata!;
  }

  void addData(String data) {
    //databaseRef.push().set({
    //  databaseRef.child('SOTR/IdDevice/ee394e83a6134e3e514f55e73ecb68d7e1f1294e/Command').set({
    databaseRef.set({
      'Data': "{\"comando\":\"ModBus\",\"tipo\":\"Energia\",\"Status\":\"Req\",\"Dato\":{\"IdDireccion\":\"" + data + "\"}}", // data,
      'TS': ServerValue.timestamp
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Firebase Demo"),
      ),
      body: FutureBuilder(
          future: _init(),
          builder: (
            BuildContext context,
            AsyncSnapshot<String> snapshot,
          ) {
            List<Widget> children;
            if (snapshot.hasData) {
              children = <Widget>[
                const Icon(
                  Icons.check_circle_outline,
                  color: Colors.green,
                  size: 60,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Text('Result: ${snapshot.data}'),
                ),
                SizedBox(height: 250.0),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    controller: textcontroller,
                  ),
                ),
                SizedBox(height: 30.0),
                Center(
                    child: RaisedButton(
                        color: Colors.pinkAccent,
                        child: Text("Save to Database"),
                        onPressed: () {
                          addData(textcontroller.text);
                          //call method flutter upload
                        })),
              ];
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            } else {
              children = const <Widget>[
                SizedBox(
                  width: 60,
                  height: 60,
                  child: CircularProgressIndicator(),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: Text('Awaiting result...'),
                )
              ];

              /*
              return Container(
                child: Column(
                  children: <Widget>[
                    // Text('Data : ${snapshot.data.toString}'),
                    Text(snapshot.data!),
                    SizedBox(height: 250.0),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        controller: textcontroller,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Center(
                        child: RaisedButton(
                            color: Colors.pinkAccent,
                            child: Text("Save to Database"),
                            onPressed: () {
                              addData(textcontroller.text);
                              //call method flutter upload
                            })),
                  ],
                ),
              );*/

            }
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: children,
              ),
            );
          }),
    );
  }
}
