import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:distribucion/app/app.dart';
import 'package:distribucion/home/home.dart';
import 'package:distribucion/home/view/my_home_page.dart';
import 'package:distribucion/notificacion/token_monitor.dart';
import 'package:distribucion/theme/view/theme_selector_screen.dart';
//import 'package:distribucion/sotrMeter/views/widgets/Pagina1.dart';
import 'package:distribucion/sotrMeter/views/sotrMeter_views.dart';
import 'package:flutter/cupertino.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  static Page page() => const MaterialPage<void>(child: HomePage());
  @override
  Widget build(BuildContext context) {
    return MyStatefulWidget();
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  String? _token;
  String _title = "Home";
  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Business',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
    Text(
      'Index 3: link',
      style: optionStyle,
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if (index == 0) {
        _title = "Home";
      }
      if (index == 1) {
        _title = "Medidores";
      }
      if (index == 2) {
        _title = "Reconectadores";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final user = context.select((AppBloc bloc) => bloc.state.user);
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
        actions: <Widget>[
          IconButton(
            key: const Key('homePage_logout_iconButton'),
            icon: const Icon(Icons.exit_to_app),
            onPressed: () => context.read<AppBloc>().add(AppLogoutRequested()),
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/Menu.jpg'),
                  // fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(Colors.black45, BlendMode.darken),
                  // image: NetworkImage('http://pic.qqtn.com/up/2016-4/2016042917263628852.jpg'),
                  //  alignment: Alignment.topCenter,
                  fit: BoxFit.fill,
                ),
                // color: Colors.blue,
              ),
              child: Text(
                'Distribucion',
                style: TextStyle(
                  //   color: Colors.white,
                  fontSize: 24,
                ),
              ),
            ),
            ListTile(
              leading: Icon(CupertinoIcons.gauge), //Icon(Icons.message),
              title: Text('Medidores'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  // MaterialPageRoute(builder: (context) => Pagina1()),
                  MaterialPageRoute(builder: (context) => SotrMeter()),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.power_off),
              title: Text('Reconectadores'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ThemeSelectorScreen()),
                );
              },
            ),
          ],
        ),
      ),
      body: Align(
        alignment: const Alignment(0, -1 / 3),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Avatar(photo: user.photo),
            const SizedBox(height: 4),
            // Text(user.email!),
            // const SizedBox(height: 4),
            Text(user.name ?? '', style: textTheme.headline5),
            const SizedBox(height: 4),
            Text(user.email ?? '', style: textTheme.headline5),
            const SizedBox(height: 4),
            // Text(user.id, style: textTheme.headline5),
            //const SizedBox(height: 4),
            //_SignUpButton(),
            const SizedBox(height: 4),
            /*//este es el token unico del celular
            TokenMonitor((token) {
              _token = token;
                return token == null ? const CircularProgressIndicator() : Text(token, style: const TextStyle(fontSize: 12));
            }),*/
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _doSalir, //() => navigateToThemeSelector(context),
        tooltip: 'SALIR',
        // child: const Icon(Icons.palette),
        child: const Icon(Icons.exit_to_app_outlined),
      ),
      /*   bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(icon: Icon(CupertinoIcons.gauge), label: 'Medidores'),
          BottomNavigationBarItem(icon: Icon(Icons.power_off), label: 'Reconectadores'),
          //BottomNavigationBarItem(icon: Icon(Icons.link), label: 'Link', backgroundColor: Colors.red),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),*/
    );
  }
}

class _SignUpButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return TextButton(
      key: const Key('loginForm_createAccount_flatButton'),
      onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage())),
      child: Text(
        'ENVIAR DATOS',
        style: TextStyle(color: theme.primaryColor),
      ),
    );
  }
}

void _doSalir(context) {
  Navigator.pop(context);
  //   BlocProvider.of<BlocAuthBloc>(context).add(
  //     DoBlocAuthEvent(emailController.text, passwordController.text),
  //  );
}

void _toprueba(context) {
  Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ThemeSelectorScreen()));
}
