import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:distribucion/app/app.dart';
import 'package:distribucion/home/home.dart';
import 'package:distribucion/home/view/my_home_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  static Page page() => const MaterialPage<void>(child: HomePage());

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final user = context.select((AppBloc bloc) => bloc.state.user);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        actions: <Widget>[
          IconButton(
            key: const Key('homePage_logout_iconButton'),
            icon: const Icon(Icons.exit_to_app),
            onPressed: () => context.read<AppBloc>().add(AppLogoutRequested()),
          )
        ],
      ),
      body: Align(
        alignment: const Alignment(0, -1 / 3),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Avatar(photo: user.photo),
            const SizedBox(height: 4),
            Text(user.email ?? '', style: textTheme.headline6),
            const SizedBox(height: 4),
            Text(user.name ?? '', style: textTheme.headline5),
            const SizedBox(height: 4),
            _SignUpButton(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home', backgroundColor: Colors.red),
          BottomNavigationBarItem(icon: Icon(Icons.business), label: 'Business', backgroundColor: Colors.red),
          BottomNavigationBarItem(icon: Icon(Icons.school), label: 'School', backgroundColor: Colors.red),
          BottomNavigationBarItem(icon: Icon(Icons.link), label: 'Link', backgroundColor: Colors.red),
        ],
        //  currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        //  onTap: _onItemTapped,
      ),
    );
  }
}

class _SignUpButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return TextButton(
      key: const Key('loginForm_createAccount_flatButton'),
      onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage())),
      child: Text(
        'ENVIAR DATOS',
        style: TextStyle(color: theme.primaryColor),
      ),
    );
  }
}
