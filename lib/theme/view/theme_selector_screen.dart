import 'package:flutter/material.dart' hide Theme;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:distribucion/theme/models/theme.dart';
import 'package:distribucion/theme/bloc/preferences_bloc.dart';
import 'package:distribucion/presentation/widgets/widget_index.dart';

import 'package:distribucion/presentation/layouts/layout_dinamic01.dart';

class ThemeSelectorScreen extends StatelessWidget {
  static const String routeName = 'theme_selector';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Theme')),
      body: BlocBuilder<PreferencesBloc, PreferencesState>(
        builder: (context, state) {
          if (state is PreferencesLoaded) {
            return LayoutDinamic(
                //return LayoutGenerico(
                //return Center(
                progressindicator: false,
                blackground: GradienteLineal(),
                child: ListView.builder(
                  itemCount: Theme.values.length,
                  itemBuilder: (context, index) {
                    return RadioListTile<Theme>(
                      onChanged: (theme) => context.read<PreferencesBloc>().add(UpdateTheme(theme!)),
                      value: Theme.values[index],
                      groupValue: state.theme,
                      title: Text(_themeToString(Theme.values[index])),
                    );
                  },
                ));
          }
          return Container();
        },
      ),
    );
  }

  String _themeToString(Theme theme) {
    switch (theme) {
      case Theme.light:
        return 'Light';
      case Theme.dark:
        return 'Dark';
      //  case Theme.mylight:
      //    return 'Mylight';
      //  case Theme.mydark:
      //    return 'Mydark';
    }
    return 'Light';
  }
}
